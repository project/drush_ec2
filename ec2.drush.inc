<?php
define('DRUSH_EC2_BASE_PATH', dirname(__FILE__));

/**
 * Implementation of hook_drush_command().
 */
function ec2_drush_command() {
  $items['ec2-launch-instance'] = array(
    'description' => '',
    'arguments' => array(
      'ami' => 'AMI.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['ec2-list-instances'] = array(
    'description' => '',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['ec2-instance-info'] = array(
    'description' => '',
    'arguments' => array(
      'site-alias' => 'A remote site alias.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['ec2-snapshot'] = array(
    'description' => '',
    'arguments' => array(
      'site-alias' => 'A remote site alias',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['ec2-resize'] = array(
    'description' => '',
    'arguments' => array(
      'site-alias' => 'A remote site alias',
      'instance-type' => '',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}

function drush_ec2_init_creds() {
  if (!defined('AWS_KEY') && isset($_SERVER['AWS_ACCESS_KEY_ID'])) {
    define('AWS_KEY', $_SERVER['AWS_ACCESS_KEY_ID']);
  }
  if (!defined('AWS_SECRET_KEY') && isset($_SERVER['AWS_SECRET_ACCESS_KEY'])) {
    define('AWS_SECRET_KEY', $_SERVER['AWS_SECRET_ACCESS_KEY']);
  }
}

function drush_ec2_get_object() {
  if (!require_once 'AWSSDKforPHP/sdk.class.php') {
    drush_print();
    drush_set_error("DRUSHEC2_CLASSNOTFOUND", dt("EC2 class not available! Please install using pear like so:"));
    drush_print("
    pear channel-discover pear.amazonwebservices.com
    pear install aws/sdk
    ");
  }

  drush_ec2_init_creds();
  // Instantiate the AmazonEC2 class
  try {
    $ec2 = new AmazonEC2();
    return $ec2;
  }
  catch (EC2_Exception $e) {
    drush_set_error('DRUSHEC2_EXCEPTION', $e->getMessage());
    exit();
  }
}

function drush_ec2_launch_instance($image_id) {
  $ec2 = new AmazonEC2();

  if (!$image_id) {
    drush_set_error(dt("Must provide an image-id argument!"));
  }
  if (!$instance_name = drush_get_option('instance-name', FALSE)) {
    drush_set_error(dt("Must provide an instance name!"));
  }


  if (!$security_group = drush_get_option('security-group', FALSE)) {
    $groups = array();
    foreach ($ec2->describe_security_groups()->body->securityGroupInfo->item as $group) {
      $groups[] = $group->groupName;
    }
    $choice = drush_choice($groups, 'Enter a number to choose which security group to use.');
    if ($choice !== FALSE) {
      $opt['SecurityGroup'] = $groups[$choice];
    }
  }

  if (!$key_pairs = drush_get_option('key-pair', FALSE)) {
    if (!is_array($ec2->describe_key_pairs()->body->keySet->item)) {
      $keypairs = array($ec2->describe_key_pairs()->body->keySet->item);
    }
    else {
      $keypairs = $ec2->describe_key_pairs()->body->keySet->item;
    }
    foreach ($keypairs as $keypair) {
      $keypair_opts[] = $keypair->keyName;
    }
    $choice = drush_choice($keypair_opts, 'Enter a number to choose which keypair to use.');
    if ($choice !== FALSE) {
      $opt['KeyName'] = $keypairs[$choice];
    }
  }

  if ($instance_type = drush_get_option('instance-type', FALSE)) {
    $opt['InstanceType'] = $instance_type;
  }

  $instance = $ec2->run_instances($image_id, 1, 1, $opt);
  $instance_id = $instance->body->instancesSet->item->instanceId;
  drush_print_r($response);

  $tag_res = $ec2->create_tags($instance_id, array('Key' => 'Name', 'Value' => $instance_name));
  drush_print_r($tag_res);
}

function drush_ec2_list_instances_validate() {
}

/**
 *
 */
function drush_ec2_instance_info($instance_id) {
  // Instantiate the AmazonEC2 class
  $ec2 = drush_ec2_get_object();
  $ec2->set_region('us-east-1');
  $ops = array('InstanceId' => $instance_id);
  foreach ($ec2->describe_instances($ops)->body->reservationSet->item->instancesSet->item as $instance) {
    drush_print_r($instance);
  }
}

function drush_ec2_list_instances() {
  $headers = array(
    'Name',
    'instanceId',
    'imageId',
    'privateDnsName',
    'dnsName',
    'keyName',
    'instanceType',
    'privateIpAddress',
    'ipAddress',
    'tagSet',
  );

  // Instantiate the AmazonEC2 class
  $ec2 = drush_ec2_get_object();

  // For some reason the AmazonEC2::REGION_US_E1 constant in the latest lib is
  // set to ec2.us-east-1.amazonaws.com' instead of us-east-1, which is needed
  // to pass to set_region().
  //$ec2->set_region(AmazonEC2::REGION_US_E1);
  $ec2->set_region('us-east-1');

  $rows = array();
  // Get the response from a call to the DescribeImages operation.
  foreach ($ec2->describe_instances()->body->reservationSet->item->instancesSet->item as $instance) {
    $row = array();
    //drush_print_r($instance);
    // Get the name of the instance from the tagSet.
    if (isset($instance->tagSet->item->key) && $instance->tagSet->item->key == 'Name') {
      $row[] = $instance->tagSet->item->value;
    }
    else {
      $row[] = "";
    }

    foreach ((array)$instance as $key => $val) {
      if (in_array($key, $headers)) {

        $row[] = $val;
      }
    }
    $rows[] = $row;
  }


  array_unshift($rows, $headers);
  drush_print_table($rows, TRUE);
}

function drush_ec2_snapshot($alias) {
  require_once 'AWSSDKforPHP/sdk.class.php';
  if ($instance = drush_ec2_get_instance($alias)) {
    // do stuff.
  }
}



function drush_ec2_resize($alias, $instance_type) {
  require_once DRUSH_EC2_BASE_PATH . '/DrushEC2.php';
  $instance = new DrushEC2Instance($alias);

  // Check for elastic IP

  // Stop running instance.
$res =  $instance->stop();
  drush_print_r($res);
  //Backup current ebs
  // TODO: Maybe create a full image with ec2-create-image?
  $res = $instance->createSnapshot();
  drush_print_r($res);

  // Change the instance type.
  $res = $instance->resize($instance_type);
  drush_print_r($res);


  // Start the instance back up.
  $instance->start();

  // Re-assign elastic ip?
  
  // Terminate old??
  //ec2-terminate-instances $instance_id
}
