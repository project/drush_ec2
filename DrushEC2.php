<?php
require_once 'AWSSDKforPHP/sdk.class.php';

class DrushEC2Instance {
  public $ec2;
  public $instance;
  public $instance_id;
  public $instance_type;
  public $root_ebs_id;

  public function __construct($alias = NULL) {
    $this->ec2 = new AmazonEC2();
    try {
      $this->instance = $this->__getInstanceFromSiteAlias($alias);
      $this->instance_id = $this->instance->instanceId;
      $this->instance_type = $this->instance->instanceType;
      $this->root_ebs_id = $this->instance->blockDeviceMapping->item->ebs->volumeId;
    }
    catch (Exception $e) {
      drush_log('OH NOES' . $e);
    }
  }

  public function __logArgs() {
    return array(
      '!instance_id' => $this->instance_id,
      '!instance_type' => $this->instance_type,
      '!root_ebs_id' => $this->root_ebs_id,
      '!date' => date('c'),
    );
  }

  public function __getInstanceFromSiteAlias($alias) {
    $site = drush_sitealias_get_record($alias);
    $hostname = drush_escapeshellarg($site['remote-host'], "LOCAL");

    // Use ip2long as a ghetto way to validate whether we have an ip or hostname.
    $ip = ip2long($hostname) === FALSE ? gethostbyname($hostname) : $hostname;

    if (empty($site['remote-host'])) {
      drush_set_error(dt('@alias is not a remote alias.', array('@alias' => $alias)));
      return;
    }

    // Instantiate the AmazonEC2 class
    foreach ($this->ec2->describe_instances()->body->reservationSet->item->instancesSet->item as $instance) {
      if ($instance->ipAddress == $ip) {
        drush_log(dt("Instance !instance matches alias !alias", array('!instance'=>$instance->instanceId,'!alias' => $alias)));
        return $instance;
      }
    }
    throw new Exception("Couldn't find a matching AMI!");
  }

  /**
   * EC2 class wrapper methods.
   */
  public function start() {
    drush_log(dt("Starting instance !instance_id…", $this->__logArgs()));
    if (!drush_get_context('DRUSH_SIMULATE')) {
      return $this->ec2->start_instances($this->instance_id);
    }
  }

  public function stop() {
    drush_log(dt("Stopping instance !instance_id…", $this->__logArgs()));
    if (!drush_get_context('DRUSH_SIMULATE')) {
      return $this->ec2->stop_instances($this->instance_id);
    }
  }

  public function resize($instance_type) {
    // ec2-modify-instance-attribute --instance-type m1.small $instance_id
    drush_log(dt("Resizing instance !instance_id from !instance_type to !new_instance_type…", $this->__logArgs() + array('!new_instance_type' => $instance_type)));
    $opt = array('InstanceType' => $instance_type);

    if (!drush_get_context('DRUSH_SIMULATE')) {
      return $this->ec2->modify_instance_attribute($this->instance_id, $opt);
    }
  }

  public function createSnapshot() {
    if (!isset($opt['Description'])) {
      $opt['Description'] = dt("EBS snapshot of instance: !instance_id on !date", $this->__logArgs());
    }

    if (!drush_get_context('DRUSH_SIMULATE')) {
      return $this->ec2->create_snapshot($this->root_eb_id, $opt = null);
    }
  }


  /**
   *
   * @param string $image_id (Required) Unique ID of a machine image, returned by a call to DescribeImages.
   * @param integer $min_count (Required) Minimum number of instances to launch. If the value is more than Amazon EC2 can launch, no instances are launched at all.
   * @param integer $max_count (Required) Maximum number of instances to launch. If the value is more than Amazon EC2 can launch, the largest possible number above minCount will be launched instead. Between 1 and the maximum number allowed for your account (default: 20).
   * @param array $opt (Optional) An associative array of parameters that can have the following keys: <ul>
   * KeyName - string - Optional - The name of the key pair.
   * SecurityGroup - string|array - Optional - The names of the security groups into which the instances will be launched. Pass a string for a single value, or an indexed array for multiple values.
   * SecurityGroupId - string|array - Optional - Pass a string for a single value, or an indexed array for multiple values.
   * UserData - string - Optional - Specifies additional information to make available to the instance(s).
   * InstanceType - string - Optional - Specifies the instance type for the launched instances. [Allowed values: t1.micro, m1.small, m1.large, m1.xlarge, m2.xlarge, m2.2xlarge, m2.4xlarge, c1.medium, c1.xlarge, cc1.4xlarge, cg1.4xlarge]
   * Placement - array - Optional - Specifies the placement constraints (Availability Zones) for launching the instances.
   *   AvailabilityZone - string - Optional - The availability zone in which an Amazon EC2 instance runs.
   *   GroupName - string - Optional - The name of the PlacementGroup in which an Amazon EC2 instance runs. Placement groups are primarily used for launching High Performance Computing instances in the same group to ensure fast connection speeds.
   *   Tenancy - string - Optional - The allowed tenancy of instances launched into the VPC. A value of default means instances can be launched with any tenancy; a value of dedicated means instances must be launched with tenancy as dedicated.
   * KernelId - string - Optional - The ID of the kernel with which to launch the instance.
   * RamdiskId - string - Optional - The ID of the RAM disk with which to launch the instance. Some kernels require additional drivers at launch. Check the kernel requirements for information on whether you need to specify a RAM disk. To find kernel requirements, go to the Resource Center and search for the kernel ID.
   * BlockDeviceMapping - array - Optional - Specifies how block devices are exposed to the instance. Each mapping is made up of a virtualName and a deviceName.
   *   x - array - This represents a simple array index.
   *   VirtualName - string - Optional - Specifies the virtual device name.
   *   DeviceName - string - Optional - Specifies the device name (e.g., /dev/sdh).
   *   Ebs - array - Optional - Specifies parameters used to automatically setup Amazon EBS volumes when the instance is launched. Takes an associative array of parameters that can have the following keys:
   *     SnapshotId - string - Optional - The ID of the snapshot from which the volume will be created.
   *     VolumeSize - integer - Optional - The size of the volume, in gigabytes.
   *     DeleteOnTermination - boolean - Optional - Specifies whether the Amazon EBS volume is deleted on instance termination.
   *   NoDevice - string - Optional - Specifies the device name to suppress during instance launch.
   * Monitoring.Enabled - boolean - Optional - Enables monitoring for the instance.
   * SubnetId - string - Optional - Specifies the subnet ID within which to launch the instance(s) for Amazon Virtual Private Cloud.
   * DisableApiTermination - boolean - Optional - Specifies whether the instance can be terminated using the APIs. You must modify this attribute before you can terminate any "locked" instances from the APIs.
   * InstanceInitiatedShutdownBehavior - string - Optional - Specifies whether the instance's Amazon EBS volumes are stopped or terminated when the instance is shut down.
   * License - array - Optional - Specifies active licenses in use and attached to an Amazon EC2 instance.
   *   Pool - string - Optional - The license pool from which to take a license when starting Amazon EC2 instances in the associated RunInstances request.
   * PrivateIpAddress - string - Optional - If you're using Amazon Virtual Private Cloud, you can optionally use this parameter to assign the instance a specific available IP address from the subnet.
   * ClientToken - string - Optional - Unique, case-sensitive identifier you provide to ensure idempotency of the request. For more information, go to How to Ensure Idempotency in the Amazon Elastic Compute Cloud User Guide.
   * AdditionalInfo - string - Optional -
   * curlopts - array - Optional - A set of values to pass directly into curl_setopt(), where the key is a pre-defined CURLOPT_* constant.
   * returnCurlHandle - boolean - Optional - A private toggle specifying that the cURL handle be returned rather than actually completing the request. This toggle is useful for manually managed batch requests.
   * @return CFResponse A object containing a parsed HTTP response.
   */

  public function launch($image_id, $min_count = 1, $max_count = 1, $opt) {
    run_instances($image_id, $min_count, $max_count, $opt = null);
  }

}
